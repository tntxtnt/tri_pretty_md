import markdown
from tri_pretty_md import PrettyMarkdown

test_str = [
'2\^12 = 4096',
'2\^(x + y) = 4096',
'2\^12 = 4096 $y^2 = x^3 + 4$',
'**2\^12** = 4096 $y^2 = x^3 + 4$',
'*2\^12* = 4096 $y^2 = x^3 + 4$',
'H~2~O Fe~2~(SO~4~)~3~',
'x~1~ = 11',
'y~1~ + y~2~ = 1234 $y_1 + y_2$',
'**y~2~** = 4096',
'*y~2~* = 4096',
]

i = 0
for s in test_str:
    print('Test #{0}'.format(i+1))
    print(s)
    print(markdown.markdown(s, extensions=[PrettyMarkdown()]))
    print()
    i += 1