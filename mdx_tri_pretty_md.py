from markdown.extensions import Extension
from markdown.inlinepatterns import SimpleTagPattern

# Start at \^ and stop at whitespace
# Examples:
#    2^10 + 1 -> 2<sup>10</sup> + 1
#    2^x+y + 1 -> 2<sup>x+y</sup> + 1
#    2^x + y + 1 -> 2<sup>x</sup> + y + 1
SUP_RE1 = r'(\\\^)([^\s\(]+)'

# Start at \^( and stop at )
# Exmple:
#    2\^(x + y) + 1 -> 2<sup>x + y</sup> + 1
SUP_RE2 = r'(\\\^\()([^\)]+)\)'

# Standard subscript with ~...~
# Example:
#    H~2~O -> H<sub>2</sub>O
SUB_RE = r'(\~)([^\~]+)\2'

def makeExtension(*args, **kwargs):
    """Inform Markdown of the existence of the extension."""
    return PrettyMarkdown(*args, **kwargs)

class PrettyMarkdown(Extension):
    def extendMarkdown(self, md, md_globals):
        sup_tag1 = SimpleTagPattern(SUP_RE1, 'sup')
        md.inlinePatterns.add('tri_superscript1', sup_tag1, '>emphasis')
        sup_tag2 = SimpleTagPattern(SUP_RE2, 'sup')
        md.inlinePatterns.add('tri_superscript2', sup_tag2, '>emphasis')
        sub_tag = SimpleTagPattern(SUB_RE, 'sub')
        md.inlinePatterns.add('tri_subscript', sub_tag, '>emphasis')