# Tri's Pretty Markdown

Python Markdown extensions for superscript and subscript.

## Usage

### Superscript

Use `\^` instead of just `^` or `^(.+)^`

- For non-whitespace content: 
  - `2\^10`  is converted to `2<sup>10</sup>`
  - `2^x+y` is converted to `2<sup>x+y</sup>`
  - `2\^x + y + 1`  is converted to `2<sup>x</sup> + y + 1` 
- For content with whitespace, wrap around `(` `)`:
  -  `2\^(x + y) + 1` is converted to `2<sup>x + y</sup> + 1`

Unfortunately, superscripts are not nested, for example `x\^y\^z` is converted to `x<sup>y\^z<sup>`

### Subscript

Just like other subscript extensions, use `~(.+)~`

- For all content (whitespace included)
  - `Fe~2~(SO~4~)~3~` is converted to `Fe<sub>2</sub>(SO<sub>4</sub>)<sub>3</sub>`
  - `f~ x + y ~` is converted to `f<sub> x + y </sub>`

Like superscripts, subscripts are not nested, for example `f~x~y~~` is converted to `f<sub>x</sub>y~~`

## License

MIT

## Why

There are [Superscript](https://github.com/jambonrose/markdown_superscript_extension) and [Subscript](https://github.com/jambonrose/markdown_subscript_extension) extensions, however, I want to use KaTeX with Pelican static gen. Pelican's `render_math` plugin doesn't support KaTeX, so I have to use `pelican_dynamic` to manually include KaTeX's CSS and JS, and leave math blocks as `$...$`. Superscript interact badly with unprocessed math blocks, for example `$y^2 = x^3$` is converted to `$y<sup>2 = x</sup>3$` and this breaks math blocks, so I make this extensions to work around that.